export class UrlUtils {
	static convertParams(params){
		let ret = []
		for(let key in params){
			let value = params[key];
			ret.push(`${key}=${value}`);
		}
		return ret.join("&")
	}

	static getUrlQuery(){
		return window.location.search;
	}

	static getUrlParam(param){
		let queryString = this.getUrlQuery();
		let urlParams = new URLSearchParams(queryString)
		return urlParams.get(param);
	}

	static getHostInfo(){
		let url = new URL(window.location.href);
		return url;
	}
}
