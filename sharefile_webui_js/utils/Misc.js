export class Misc {
	static formatFileSize = function(size){
	    let i = Math.floor( Math.log(size) / Math.log(1024) );
	    return size==0 ? "0B" :( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
	}
	
	static copyToClipboard(text, element = document.body) {
		let textarea = element.appendChild(document.createElement("textarea"));
		textarea.value = text;
		textarea.focus();
		textarea.select();
		textarea.style.width = "0 px";
		document.execCommand('copy');
		textarea.parentNode.removeChild(textarea);
	}
}