export class RestApiClient{
	static BASE_API_URL = "api";

	constructor(command, urlParams = {}, formData = {}) {
		this.command = command;
		this.urlParams = urlParams;
		this.formData = formData;
	}

	_getApiCallUrl(){
		let urlParamsList = [];
		Object.entries(this.urlParams).map(([key, value]) => {
			let encodedValue = encodeURIComponent(value);
			urlParamsList.push(`${key}=${encodedValue}`);
		});
		let urlParamsStr = urlParamsList ? "?"+urlParamsList.join("&") : "";
		return `${RestApiClient.BASE_API_URL}/${this.command}${urlParamsStr}`;
	}

	_fetch(method, callback = null){
		const apiUrl = this._getApiCallUrl();
		const requestConfig = {
			method: method,
		}

		if( Object.keys(this.formData).length ){
			let formData = new FormData();
			for(let key in this.formData) {
				formData.append(key, this.formData[key]);
			}
			requestConfig["body"] = formData;
		}

		fetch(apiUrl, requestConfig)
		.then((response)=> {
			if(response.status == 200) return response.json();
			response.json().then((data) => {
				let message = data.message || "Error message has not been provided.";
				alert(`Error with status code '${response.status}': ${message}`);
			}).catch((error) => {
				alert(`Error with status code '${response.status}' with no JSON data: ${error}`);
			});
		})
		.then((data) => {
				if(callback) callback(data);
			})
		.catch((error)=>{
			alert(`Error while fetching data: ${error}`);
		});
	}

	get(callback = null){
		return this._fetch("GET", callback)
	}

	post(callback = null){
		return this._fetch("POST", callback)
	}

	put(callback = null){
		return this._fetch("PUT", callback)
	}

	delete(callback = null){
		return this._fetch("DELETE", callback)
	}
}