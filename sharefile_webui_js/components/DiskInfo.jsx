import React, { Component } from "react";
import { Misc } from "../utils/Misc";
import {RestApiClient} from "../utils/RestApiClient";

export class DiskInfo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			free: "...",
			used: "...",
			total: "..."
		};
	}

	_loadData(){
		new RestApiClient("disk/")
			.get((data) => {
				this.setState( data.diskUsage );
			});
	}

	componentDidMount() {
		this._loadData();
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if(this.props.context != prevProps.context){
			this._loadData();
		}
	}

	render() {
		return (
			<div id="disk-info">
				<div>Storage -</div>
				<div className="free">free: {Misc.formatFileSize(this.state.free)},</div>
				<div className="used">used: {Misc.formatFileSize(this.state.used)},</div>
				<div className="total">total: {Misc.formatFileSize(this.state.total)}</div>
			</div>
		)
	}
}