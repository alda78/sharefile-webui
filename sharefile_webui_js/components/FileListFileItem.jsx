import React, { Component } from "react";
import PropTypes from "prop-types";
import { Misc } from "../utils/Misc";
import {RestApiClient} from "../utils/RestApiClient";
import {UrlUtils} from "../utils/UrlUtils";

export class FileListFileItem extends Component {
	constructor(props) {
		super(props);
		this.props = {
			item: {
				name: "",
				path: "",
				token: "",
				size: 0,
				mdatetimeISO: "",
                accessNum: 0,
				isEditable: false
			}
		};

		const hostInfo = UrlUtils.getHostInfo();
		const hotsPort = hostInfo.port ? `:${hostInfo.port}` : ""
		this.appURL = `${hostInfo.protocol}//${hostInfo.hostname}${hotsPort}`;
	}

    static get propTypes() {
        return {
            item: PropTypes.object,
            name: PropTypes.string,
            path: PropTypes.string,
            token: PropTypes.string,
            size: PropTypes.string,
            mdatetimeISO: PropTypes.string,
            accessNum: PropTypes.bigint,
            isEditable: PropTypes.string,
            copyMethod: PropTypes.string,
            appOpenViewerHandler: PropTypes.func,
            appOpenEditorHandler: PropTypes.func,
        };
    }

    getTextForClipboard = (addLineBreak = false) => {
        const { path, name, token } = this.props.item;
        if(!token) return null;

		const fullUrl = `${this.appURL}/share/${path}?token=${token}`;
		const fullDownloadUrl = `${this.appURL}/download/share/${path}?token=${token}`;
		let textForClipboard = null;
		switch (this.props.copyMethod) {
			case "url": textForClipboard = fullUrl + (addLineBreak ? "\r\n" : ""); break;
			case "url-label": textForClipboard = `${name} - ${fullUrl}` + (addLineBreak ? "\r\n" : "");break;
			case "md": textForClipboard = `[${name}](${fullUrl})` + (addLineBreak ? "  \r\n" : ""); break;
			case "html": textForClipboard = `<a href="${fullUrl}">${name}</a>` + (addLineBreak ? "<br/>\r\n" : ""); break;
			case "download": textForClipboard = `${fullDownloadUrl}` + (addLineBreak ? "\r\n" : "");break;
			case "download-label": textForClipboard = `${name} - ${fullDownloadUrl}` + (addLineBreak ? "\r\n" : "");break;
			case "curl": textForClipboard = `curl "${fullUrl}" -o "${name}"` + (addLineBreak ? "\r\n" : ""); break;
			default: textForClipboard = fullUrl;
		}
        return textForClipboard;
    }

	viewFileClick = () => {
		const path = this.props.item.path;
		this.props.appOpenViewerHandler(path);
	}

	editFileClick = () => {
		const path = this.props.item.path;
		this.props.appOpenEditorHandler(path);
	}

	copyUrlClick = () => {
        let textForClipboard = this.getTextForClipboard();
		Misc.copyToClipboard(textForClipboard, event.currentTarget);
	}

	newTokenClick = () => {
		const path = this.props.item.path;
		new RestApiClient(`token/${path}`)
			.post((data) => {
				this.props.fileListLoadDataHandler();
			});
	}

	deleteTokenClick = () => {
		const path = this.props.item.path;
		new RestApiClient(`token/${path}`)
			.delete((data) => {
				this.props.fileListLoadDataHandler();
			})
	}
	
	renameClick = () => {
		const { path, name } = this.props.item;
		const newFileName = prompt(`Rename file '${name}'`, name);
		if(newFileName) {
			new RestApiClient(`file/${path}`, {"rename": newFileName})
				.put((data) => {
					this.props.fileListLoadDataHandler();
				});
		}
	}

	deleteClick = () => {
		const path = this.props.item.path;
		new RestApiClient(`file/${path}`)
			.delete((data) => {
				this.props.fileListLoadDataHandler();
                this.props.appForceRenderDiskInfoHandler();
			});
	}

	render(){
		const item = this.props.item;
		const className = item.token ? "file-container-token" : "file-container";
		const renderDivFile = (item) => {
			const linkUrl = `/share/${item.path}?token=${item.token}`
			if(item.token) {
				return (
					<div className="file" data-name={item.name}><a href={linkUrl} target="_blank">{item.name}</a></div>
				)
			} else {
				return (
					<div className="file" data-name={item.name}>{item.name}</div>
				)
			}
		};

		const renderButtonList = (item) => {
		let buttonsList = [];

		if(item.isEditable) {
			buttonsList.push(<button class="file-button-view-file" onClick={this.viewFileClick} title="View file"><img src="/static/img/eye-regular.svg"/></button>);
			buttonsList.push(<button class="file-button-edit-file" onClick={this.editFileClick} title="Edit file"><img src="/static/img/edit-regular.svg"/></button>);
		}
		if(item.token){
			buttonsList.push(<button class="file-button-copy-url" onClick={this.copyUrlClick} title="Copy link"><img src="/static/img/copy-regular.svg"/></button>);
			buttonsList.push(<button class="file-button-new-token" onClick={this.newTokenClick} title="New token"><img src="/static/img/share-alt-solid.svg"/></button>);
			buttonsList.push(<button class="file-button-delete-token" onClick={this.deleteTokenClick} title="Delete token"><img src="/static/img/share-alt-square-solid.svg"/></button>);
		} else {
			buttonsList.push(<button class="file-button-new-token" onClick={this.newTokenClick} title="Add token"><img src="/static/img/share-alt-solid.svg"/></button>);
			buttonsList.push(<button class="file-button-rename-file" onClick={this.renameClick} title="Rename file"><img src="/static/img/i-cursor-solid.svg"/></button>);
		}
		buttonsList.push(<button class="file-button-delete-file" onClick={this.deleteClick} title="Delete file"><img src="/static/img/trash-solid.svg"/></button>);
		return buttonsList;
	}

		return (
			<div className={className}>
				{ renderDivFile(item) }
				<div className="filedate">{item.mdatetimeISO}</div>
				<div className="filesize">{Misc.formatFileSize(item.size)}</div>
				<div className="fileaccessnum" title="Number of file access">{item.accessNum}x</div>
				<div className="img-buttons">
					{
						renderButtonList(item).map((buttonItem, index) => {
							return buttonItem;
						})
					}
				</div>
			</div>
			)
	}
}
