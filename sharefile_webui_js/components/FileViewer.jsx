import React, { Component } from "react";
import {RestApiClient} from "../utils/RestApiClient";

export class FileViewer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			path: "",
			name: "",
			content: "",
		};
	}

	_loadData() {
		const path = this.props.path;
		new RestApiClient(`filecontent/${path}`, {"decode-content": 1})
			.get((data) => {
				this.setState({ 
					path: data.path,
					name: data.name,
					content: data.content
				});
			});
	}

	componentDidMount() {
		this._loadData();
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if(this.state.content != prevState.content){
			// make link open at new tab
			document.querySelectorAll("#file-viewer-content a").forEach((element)=>{
			    element.setAttribute("target", "_blank");
			});
		}
	}

	closeClick = () => {
		this.props.appCloseViewerHandler();
	}

	render() {
		return (
			<div id="file-editor">
				<div className="file-editor-title">Viewing "{this.state.name}"</div>
				<div id="file-viewer-content" dangerouslySetInnerHTML={ {__html: this.state.content} }></div>
				<div className="file-viewer-controls">
					<button onClick={this.closeClick}>Close</button>
				</div>
			</div>
		)
	}
}
