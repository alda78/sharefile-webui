import React, { Component } from "react";
import {RestApiClient} from "../utils/RestApiClient";

export class FileEditor extends Component {
	constructor(props) {
		super(props);
		this.state = {
			path: "",
			name: "",
			content: "",
		};
		this.fileEditorReference = React.createRef();
	}

	_loadData() {
		const path = this.props.path;
		new RestApiClient(`filecontent/${path}`)
			.get((data) => {
				this.setState({ 
					path: data.path,
					name: data.name,
					content: data.content
				});
			});
	}

	componentDidMount() {
		this._loadData();
	}
	
	saveClick = () => {
		const path = this.state.path;
		const content = this.fileEditorReference.current.value;
		new RestApiClient(`filecontent/${path}`, {}, {"content": content})
			.put((data) => {
				this.props.appForceRenderFileListHandler();
				this.props.appCloseEditorHandler();
			});
	}

	cancelClick = () => {
		this.props.appCloseEditorHandler();
	}

	render() {
		return (
			<div id="file-editor">
				<div className="file-editor-title">Editing "{this.state.name}"</div>
				<textarea ref={this.fileEditorReference} id="file-editor-content" defaultValue={this.state.content} />
				<div className="file-editor-controls">
					<button onClick={this.saveClick}>Save "{this.state.name}"</button>
					<button onClick={this.cancelClick}>Cancel</button>
				</div>
			</div>
		)
	}
}
