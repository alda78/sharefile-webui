import React, { Component } from "react";
import {RestApiClient} from "../utils/RestApiClient";

export class FileListDirItem extends Component {
	constructor(props) {
		super(props);
		this.props = {
			item: {
				name: "",
				path: "",
			}
		};
	}

	appChangeContextClick = () => {
		this.props.appChangeContextHandler(this.props.item.path, this.props.item.pathLabel);
	}

	renameClick = () => {
		const path = this.props.item.path;
		const name = this.props.item.name;
		const newFileName = prompt(`Rename directory '${name}'`, name);
		if(newFileName) {
			new RestApiClient(`dir/${path}`, {"rename": newFileName})
				.put((data)=> { 
					this.props.fileListLoadDataHandler();
				});
		}
	}

	deleteClick = () => {
		const path = this.props.item.path;
		new RestApiClient(`dir/${path}`)
			.delete((data) => {
				this.props.fileListLoadDataHandler();
			});
	}

	render() {
		const item = this.props.item;
		if(item.name == "/" || item.name == ".."){
			return (
				<div class="dir-container">
					<div class="dir" onClick={this.appChangeContextClick}>{item.name}</div>
					<div class="img-buttons"></div>
				</div>
			)
		}
		return (
				<div className="dir-container">
					<div className="dir"  data-name={item.name} onClick={this.appChangeContextClick}>{item.name}</div>
					<div className="img-buttons">
						<button className="dir-button-rename-dir" onClick={this.renameClick} title="Rename dir"><img src="/static/img/i-cursor-solid.svg"/></button>
						<button className="dir-button-delete-dir" onClick={this.deleteClick} title="Delete dir"><img src="/static/img/trash-solid.svg"/></button>
					</div>
				</div>
			)
	}
}
