import React, { Component } from "react";
import {RestApiClient} from "../utils/RestApiClient";

export class HeaderControls extends Component {
	constructor(props) {
		super(props);
	}

	copyMethodRadioButtonChanged = (event) => {
		this.props.appChangeCopyMethodHandler(event.target.value);
	}

	addDirClick = () => {
		const name = "New directory";
		new RestApiClient(`dir/${this.props.context}/${name}`)
			.post((data) => {
				this.props.appForceRenderFileListHandler();
			});
	}

	addFileClick = () => {
		const name = "New file"
		new RestApiClient(`filecontent/${this.props.context}/${name}`)
			.post((data) => {
				this.props.appForceRenderFileListHandler();
			});
	}

	render() {
		return (
			<div id="controls">
				<div className="control-item" onChange={this.copyMethodRadioButtonChanged}>
					<span>Copy link as:</span>
					<input type="radio" name="radio-copy-as" value="url" checked={this.props.copyMethod == "url"}  title="Copy simple URL" /> <label htmlFor="copy-as-url">URL</label>
					<input type="radio" name="radio-copy-as" value="url-label" checked={this.props.copyMethod == "url-label"}  title="Copy URL with filename label" /> <label htmlFor="copy-as-url-label">lblURL</label>
					<input type="radio" name="radio-copy-as" value="md" checked={this.props.copyMethod == "md"}   title="Copy as MarkDown link" /><label htmlFor="copy-as-md">MD</label>
					<input type="radio" name="radio-copy-as" value="html" checked={this.props.copyMethod == "html"} title="Copy as HTML link" /><label htmlFor="copy-as-html">HTML</label>
					<input type="radio" name="radio-copy-as" value="download" checked={this.props.copyMethod == "download"} title="Copy as link for download page" /><label htmlFor="copy-as-download">Download</label>
					<input type="radio" name="radio-copy-as" value="download-label" checked={this.props.copyMethod == "download-label"} title="Copy as link for download page with filename label" /><label htmlFor="copy-as-download-label">lblDownload</label>
					<input type="radio" name="radio-copy-as" value="curl" checked={this.props.copyMethod == "curl"} title="Copy as CURL" /><label htmlFor="copy-as-curl">CURL</label>
				</div>
				<div className="control-item img-buttons">
                    <button onClick={()=>this.props.appCopyAllUrlHandler()} title="Copy all links from current directory"><img src="/static/img/copy-regular.svg"/></button>
					<button onClick={this.addDirClick} title="Add new dir"><img src="/static/img/folder-plus-solid.svg"/></button>
					<button onClick={this.addFileClick} title="Create new text file"><img src="/static/img/file-alt-solid.svg"/></button>
				</div>
			</div>
		)
	}
}
