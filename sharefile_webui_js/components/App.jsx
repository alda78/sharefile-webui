import "whatwg-fetch";
import React, { Component } from "react";
import {DiskInfo} from "./DiskInfo";
import {HeaderControls} from "./HeaderControl";
import {FileEditor} from "./FileEditor";
import {FileList} from "./FileList";
import {FileUpload} from "./FileUpload";
import {UrlUtils} from "../utils/UrlUtils"
import {FileViewer} from "./FileViewer";
import {Misc} from "../utils/Misc";

export class App extends Component {
	constructor(props) {
		super(props);
		const urlContext = UrlUtils.getUrlParam("context");
		this.state = {
			context: urlContext ? urlContext : "",
			contextLabel: "",
			copyMethod: "url",
			edit: { path: "" },
			view: { path: "" },
		};
		this.fileListReference = React.createRef(); // create reference for FileList component to be able forcing data reload
		this.diskInfoReference = React.createRef(); // create reference for DiskInfo component to be able forcing data reload
		window.history.pushState({"context": this.state.context}, "", `?context=${this.state.context}`);
	}



	appChangeContextHandler = (context, contextLabel) => {
		this.setState(Object.assign(this.state, {context: context, contextLabel: contextLabel }));
		window.history.pushState({"context": this.state.context}, "", `?context=${this.state.context}`);
	}

	appChangeCopyMethodHandler = (copyMethod) => {
		this.setState(Object.assign(this.state, {copyMethod: copyMethod }));
	}

    appCopyAllUrlHandler = () => {
        let textForClipboard = this.fileListReference.current.getTextForClipboard();
        Misc.copyToClipboard(textForClipboard, event.currentTarget);
    }

	appForceRenderFileListHandler = () => {
		this.fileListReference.current._loadData();
	}

	appForceRenderDiskInfoHandler = () => {
		this.diskInfoReference.current._loadData();
	}

	appOpenEditorHandler = (path) => {
		this.setState( Object.assign(this.state, { edit: { path: path }}) );
	}

	appCloseEditorHandler = () => {
		this.setState( Object.assign(this.state, { edit: { path: "" }}) );
	}

	appOpenViewerHandler = (path) => {
		this.setState( Object.assign(this.state, { view: { path: path }}) );
	}

	appCloseViewerHandler = () => {
		this.setState( Object.assign(this.state, { view: { path: "" }}) );
	}

	render(){
		return (
			<div className="app">
				<div className="app-header">
					<div className="app-header-top">
						<div id="dir-context">{this.state.contextLabel}</div>
						<DiskInfo ref={this.diskInfoReference} />
					</div>
					<div className="app-header-bottom">
						<HeaderControls 
							context={this.state.context}
							copyMethod={this.state.copyMethod}
						    appChangeCopyMethodHandler={this.appChangeCopyMethodHandler}
							appForceRenderFileListHandler={this.appForceRenderFileListHandler}
                            appCopyAllUrlHandler={this.appCopyAllUrlHandler}
						/>
					</div>
				</div>
                <div className="app-filelist">
                    <FileList
                        ref={this.fileListReference}
                        context={this.state.context}
                        copyMethod={this.state.copyMethod}
                        appChangeContextHandler={this.appChangeContextHandler}
                        appOpenViewerHandler={this.appOpenViewerHandler}
                        appOpenEditorHandler={this.appOpenEditorHandler}
                        appForceRenderDiskInfoHandler={this.appForceRenderDiskInfoHandler}
                    />
                </div>
                <div className="app-fileupload">
                    <FileUpload
                        context={this.state.context}
                        appForceRenderDiskInfoHandler={this.appForceRenderDiskInfoHandler}
                        appForceRenderFileListHandler={this.appForceRenderFileListHandler}
                    />
                </div>
                <div className="app-fileeditor">
                    {
                        this.state.view.path &&
                            <FileViewer
                                path={this.state.view.path}
                                appCloseViewerHandler={this.appCloseViewerHandler}
                            />
                    }
    
                    {
                        this.state.edit.path &&
                            <FileEditor
                                path={this.state.edit.path}
                                appCloseEditorHandler={this.appCloseEditorHandler}
                                appForceRenderFileListHandler={this.appForceRenderFileListHandler} 
                            />
                    } 
                </div>
			</div>
		)
	}
}
