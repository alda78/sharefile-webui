import React, { Component } from "react";
import PropTypes from "prop-types";

export class FileUpload extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.dropzone = null;
	}
    
    static get propTypes() {
        return {
            appForceRenderDiskInfoHandler: PropTypes.func,
            appForceRenderFileListHandler: PropTypes.func,
        };
    }

	_renderDropZone(){
		if(this.dropzone != null) return;
        console.log("FileUpload._renderDropZone");

		Dropzone.autoDiscover = false;
		this.dropzone = new Dropzone("#fileupload", { url: `/api/multiupload/${this.props.context}`});
		this.dropzone.options.paramName = 'file';
		this.dropzone.options.chunking = true;
		this.dropzone.options.forceChunking = true;
		this.dropzone.options.maxFilesize = 8*1024; // megabytes = 8 GB
		this.dropzone.options.chunkSize = 5*1024*1024 // bytes = 5 MB
		this.dropzone.options.autoProcessQueue = true;
		this.dropzone.options.paralelUploads = false;
		this.dropzone.on("complete", (event)=>{
            console.log(`FileUpload.dropzone.complete: event=${event}`);
			this.props.appForceRenderDiskInfoHandler();
			this.props.appForceRenderFileListHandler();
		});
		this.dropzone.on("queuecomplete", (event)=>{
            console.log(`FileUpload.dropzone.queuecomplete: event=${event}`);
			this._destroyDropZone();
			this._renderDropZone();
		});
	}

	_destroyDropZone(){
		if(this.dropzone == null) return;

        console.log("FileUpload._destroyDropZone");
        this.dropzone.destroy();
        this.dropzone = null;
	}

	componentDidMount() {
        console.log("FileUpload.componentDidMount");
		this._destroyDropZone();
		this._renderDropZone();
	}

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.context !== this.props.context){
            console.log("FileUpload.componentDidUpdate / context changed");
            this._destroyDropZone();
            this._renderDropZone();
        }
    }

    render() {
		return (
			<div id="fileupload" className="dropzone"></div>
		)
	}
}