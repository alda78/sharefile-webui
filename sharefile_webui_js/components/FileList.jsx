import React, { Component } from "react";
import {FileListFileItem} from "./FileListFileItem";
import {FileListDirItem} from "./FileListDirItem";
import {RestApiClient} from "../utils/RestApiClient";

export class FileList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			context: "",
			contextLabel: "",
			fileList: [],
		};
        this.fileListFileItemReferenceList = [];
        this.textForClipboard = "";
	}

	_loadData() {
		const command = `dir/${this.props.context}`;
		new RestApiClient(command)
			.get(( data )=>{
				this.setState({
						context: data.context,
						contextLabel: data.contextLabel,
						fileList: data.result,
					});
				this.props.appChangeContextHandler(data.context, data.contextLabel); // update current context for page reload case
			});
	}

    getTextForClipboard() {
        let textForClipboard = [];
        this.fileListFileItemReferenceList.map((item, index) => {
            if(item.current && item.current.getTextForClipboard){
                let text = item.current.getTextForClipboard(true);
                if(text) {
                    textForClipboard.push(text); 
                }
            }
        });
        return textForClipboard.join("");
    }


	fileListLoadDataHandler = () => {
		this._loadData();
	}

	componentDidMount() {
		this._loadData();
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if(this.props.context != prevProps.context){
			this._loadData();
		}
	}

	render() {
        this.fileListFileItemReferenceList = [];
		return (
			this.state.fileList.map((item, index) => {
				if(item.type == "file"){
                    // render
                    let reference = React.createRef();
                    this.fileListFileItemReferenceList.push(reference);
					return (
						<FileListFileItem 
                            ref={reference}
							item={item}
							copyMethod={this.props.copyMethod}
							fileListLoadDataHandler={this.fileListLoadDataHandler}
							appOpenViewerHandler={this.props.appOpenViewerHandler}
							appOpenEditorHandler={this.props.appOpenEditorHandler}
                            appForceRenderDiskInfoHandler={this.props.appForceRenderDiskInfoHandler}
						/>
					)
				} else {
					return (
						<FileListDirItem
							item={item}
							appChangeContextHandler={this.props.appChangeContextHandler}
							fileListLoadDataHandler={this.fileListLoadDataHandler}
						/>
					)
				}
			})
		)
	}
}
